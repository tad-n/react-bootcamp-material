function getMood() {
	const moods = [ 'Angry', 'Hungry', 'Silly', 'Quiet', 'Paranoid' ];
	return moods[Math.floor(Math.random() * moods.length)];
}
class JSXDemo extends React.Component {
	render() {
		return (
			<div>
				<h1>My Current Mood is: {getMood()}</h1>
				<img src="https://images.unsplash.com/photo-1547626740-02cb6aed9ef8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80" />
			</div>
		);
	}
}

ReactDOM.render(<JSXDemo />, document.getElementById('root'));
